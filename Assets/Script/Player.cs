﻿using UnityEngine;
using UnityEngine.EventSystems;
using Mirror;
using UnityEngine.UI;

public class Player : NetworkBehaviour
{
    private Joystick[] joyStick;
    protected Joystick joystickMove;
    protected Joystick joystickRot;
    protected JoyButton joybutton;

    protected bool pick;

    private Rigidbody2D rigidbody;
    [SerializeField] private float moveSpeed = 8.0f;
    [SerializeField] private float rotSpeed = 5.0f;

    // Start is called before the first frame update
    void Start()
    {
        joyStick = FindObjectsOfType<Joystick>();
        joystickRot = joyStick[1];
        joystickMove = joyStick[0];

        rigidbody = GetComponent<Rigidbody2D>();

#if UNITY_EDITOR || UNITY_STANDALONE
        joystickMove.GetComponent<Image>().enabled = false;
        joystickMove.transform.GetChild(0).GetComponent<Image>().enabled = false;
        joystickRot.GetComponent<Image>().enabled = false;
        joystickRot.transform.GetChild(0).GetComponent<Image>().enabled = false;
#endif
    }

    [Client]
    void Update()
    {
        if (!hasAuthority) { return; }

        if (!joystickMove) { return; }

        if (!joystickRot) { return; }

        if (!Input.anyKey) { return; }

        CmdMove();
    }

    [Command]
    void CmdMove()
    {
        RpcMove();
    }

    [ClientRpc]
    private void RpcMove()
    {
#if UNITY_ANDROID
        MovePlayerAndroid();
        PlayerFacingAndroid();
#endif
#if UNITY_STANDALONE || UNITY_EDITOR
        MovePlayer();
        PlayerFacing();
#endif
    }

#if UNITY_EDITOR || UNITY_STANDALONE
    private void MovePlayer()
    {
        float horizontalMove = Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime;
        float verticalMove = Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime;

        Vector3 movementDirection = new Vector3(horizontalMove, verticalMove, 0);

        rigidbody.MovePosition(transform.position + movementDirection);
    }
    private void PlayerFacing()
    {
        var mouse = Input.mousePosition;
        var screenPoint = Camera.main.WorldToScreenPoint(transform.localPosition);
        var offset = new Vector3(mouse.x - screenPoint.x, mouse.y - screenPoint.y, mouse.z - screenPoint.z);
        var angle = Mathf.Atan2(offset.x, offset.y) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0, 0, -angle);
    }
#endif

#if UNITY_ANDROID
    private void MovePlayerAndroid()
    {
        float horizontalMove = joystickMove.Horizontal * moveSpeed * Time.deltaTime;
        float verticalMove = joystickMove.Vertical * moveSpeed * Time.deltaTime;

        Vector3 movementDirection = new Vector3(horizontalMove, verticalMove, 0);

        rigidbody.MovePosition(transform.position + movementDirection);
    }
    private void PlayerFacingAndroid()
    {
        float horizontalRot = joystickRot.Horizontal * rotSpeed;
        float verticalRot = joystickRot.Vertical * rotSpeed;

        var angle = Mathf.Atan2(-horizontalRot, verticalRot) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0, 0, angle);
    }
#endif

}
