﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{
    public GameObject terinjak1, belumTerinjak1;
    public GameObject terinjak2, belumTerinjak2;
    public GameObject selbuka, seltutup;

    private bool leverTrigger = false;

    void Start()
    {
        LeverTrigger();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            leverTrigger = true;
            Debug.Log(leverTrigger);
        }

        LeverTrigger();
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            leverTrigger = false;
            Debug.Log(leverTrigger);
        }
        LeverTrigger();
    }

    private void LeverTrigger()
    {
        if (leverTrigger)
        {
            terinjak1.SetActive(true);
            terinjak2.SetActive(true);
            selbuka.SetActive(true);
            belumTerinjak1.SetActive(true);
            belumTerinjak2.SetActive(true);
            seltutup.SetActive(false);
            Debug.Log("Nabrakk");
        }
        else
        {
            terinjak1.SetActive(false);
            terinjak2.SetActive(false);
            selbuka.SetActive(false);
            belumTerinjak1.SetActive(true);
            belumTerinjak2.SetActive(true);
            seltutup.SetActive(true);
            Debug.Log("Nutupp");
        }
    }
}
