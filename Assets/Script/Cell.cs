﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Mirror;

public class Cell : NetworkBehaviour
{
    public GameObject terinjak1, belumTerinjak1;
    public GameObject terinjak2, belumTerinjak2;
    public GameObject selbuka, seltutup;
    public GameObject gerbang, key;

    [SyncVar] bool leverStepped_server = false;
    private bool leverStepped_local = false;

    public override void OnStartServer()
    {
        base.OnStartServer();
        LeverBehaviour();
    }

    // Start is called before the first frame update
    void Start()
    {
        LeverBehaviour();
    }

    // Update is called once per frame
    void Update()
    {
        CmdLever();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            leverStepped_local = true;
            Debug.Log("stepping" + leverStepped_local);
        } else
        {
            leverStepped_local = false;
        }
    }

    [Command]
    private void CmdLever()
    {
        if (leverStepped_local)
        {
            leverStepped_server = true;
            Debug.Log("stepping_server" + leverStepped_server);
        } else
        {
            leverStepped_server = false;
        }

        RpcCell();
    }

    [ClientRpc]
    private void RpcCell()
    {
        LeverBehaviour();
    }

    private void LeverBehaviour()
    {
        if (leverStepped_server)
        {
            terinjak1.SetActive(true);
            terinjak2.SetActive(true);
            selbuka.SetActive(true);
            belumTerinjak1.SetActive(false);
            belumTerinjak2.SetActive(false);
            seltutup.SetActive(false);
        }
        else
        {
            terinjak1.SetActive(false);
            terinjak2.SetActive(false);
            selbuka.SetActive(false);
            belumTerinjak1.SetActive(true);
            belumTerinjak2.SetActive(true);
            seltutup.SetActive(true);
        }
    }
}
